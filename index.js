let express = require('express');
let app = express();
const sequelize = require('./config');
let nunjucks = require('nunjucks');
let port = 3001;

let PATH_TO_FILE = './static/public/templates';

app.use(express.json());

const User = require('./model/User');
const Analytics = require('./model/Analytics');
const Domains = require('./model/Domains');


Domains.addDomain = (req, res) => {
  const {url} = req.body;
  Domains.create({url})
    .then((domain) => {
      res.json(domain);
    })
};

Domains.listAll = (req, res) => {
  Domains.findAll({where: {}})
    .then(doms => {
      res.json(doms);
    })
};

Analytics.getAllEntries = (req, res) => {
  Analytics.findAll({where: {}})
    .then((analytics) => {
      res.json(analytics);
    })
};

Analytics.addEntry = (req, res) => {
  const {visitedAt, url} = req.body;
  console.log(req.params);
  const {id} = req.params;
  console.log(id);
  Domains.findByPk(id)
    .then(entry => {
      console.log(entry);
      if (entry.url === url) {
        Analytics.create({visitedAt, url})
          .then(analytic => {
            res.json(analytic);
          })
          .catch(err => console.log('why??????', err));
      }
    })
    .catch(err => console.log('rofl', err));
};

// Create

User.createUser = (req, res) => {
  console.log(req.body);
  const {firstName, lastName, email, password} = req.body;
  User.create({firstName, lastName, email, password})
    .then(user => {
      res.json(user);
    });
};

// Read

User.getUsersAll = (req, res) => {
  User.findAll({where: {}})
    .then(users => {
      console.log('in get all');
      res.json(users);
    })
};

User.getUserById = (req, res) => {
  User.findByPk(req.params.id)
    .then(user => {
      console.log('In here');
      res.json(user);
    })
};

// Update

User.updateUserById = (req, res) => {
  const {id} = req.params;
  const {firstName, lastName, email, password} = req.body;
  User.update({firstName, lastName, email, password}, {where: {id}, returning: true})
    .then((user) => {
      console.log(user);
      res.json(user);
    })
};

// Delete

User.deleteUserById = (req, res) => {
  const {id} = req.params;
  User.destroy({where: {id}})
    .then(() => {
      console.log('User successfully destroyed. Id: ' + id);
      res.send('success');
    })
};

app.route('/domains')
  .post(Domains.addDomain)
  .get(Domains.listAll);

app.route('/user')
  .get(User.getUsersAll)
  .post(User.createUser);

app.route('/user/:id')
  .get(User.getUserById)
  .put(User.updateUserById)
  .delete(User.deleteUserById);

app.route('/track')
  .post(Analytics.addEntry)
  .get(Analytics.getAllEntries);

app.route('/track/:id')
  .post(Analytics.addEntry);

app.get('/', (req, res) => {
  res.render('index.njk', {data: {}});
});

app.use(express.static('./static/public'));

nunjucks.configure(PATH_TO_FILE, {
  express: app,
  autoescape: true
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
