const sequelize = require('../config');
const {DataTypes, Model} = require('sequelize');

class Domains extends Model {}

Domains.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  url: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {sequelize, modelName: 'domains', timestamps: false});

module.exports = Domains;
