const sequelize = require('../config');
const {Model, DataTypes} = require('sequelize');

class Analytics extends Model {}

Analytics.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  visitedAt: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  url: {
    type: DataTypes.STRING,
    allowNull: false
  }

}, {sequelize, modelName: 'statistics', timestamps: false});

module.exports = Analytics;
